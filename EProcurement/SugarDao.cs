﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OracleSugar;
using System.Configuration;

namespace EProcurement
{
    /// <summary>
    /// SqlSugar
    /// </summary>
    public class SugarDao
    {
        private SugarDao()
        {
        }
        public static string ConnectionString
        {
            get
            {
                string reval = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                return reval;
            }
        }
        public static SqlSugarClient GetInstance()
        {
            var db = new SqlSugarClient(ConnectionString);
            db.IsEnableLogEvent = true;//Enable log events
            db.LogEventStarting = (sql, par) => {
                Console.WriteLine(sql + " " + par + "\r\n");
            };
            return db;
        }
    }
}
