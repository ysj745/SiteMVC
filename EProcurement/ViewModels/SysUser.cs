﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;

namespace EProcurement.ViewModels
{
    /// <summary>
    /// 用户
    /// </summary>
    public class SysUser:SM_USER
    {
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string PURCODE { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string PURNAME { get; set; }
    }
}
