﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EProcurement.ViewModels
{
    /// <summary>
    /// 供应商信息
    /// </summary>
    public class SupplierInfo
    {
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 纳税人登记号
        /// </summary>
        public string Taxpayerid{ get; set; }
        /// <summary>
        /// 注册资金
        /// </summary>
        public string Registerfund { get; set; }
        /// <summary>
        /// 经济类型
        /// </summary>
        public string Ecotypesincevfive { get; set; }
        /// <summary>
        /// 法人
        /// </summary>
        public string Legalbody { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Corpaddress { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel1 { get; set; }
    }
}
