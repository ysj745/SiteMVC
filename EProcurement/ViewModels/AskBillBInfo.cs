﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;

namespace EProcurement.ViewModels
{
    public class AskBillBInfo:PURP_ASKBILL_B
    {
        /// <summary>
        /// 询报价单号
        /// </summary>
        public string VbillCode { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string PurName { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }
        /// <summary>
        /// 物料型号
        /// </summary>
        public string MaterialType { get; set; }
        /// <summary>
        /// 图号
        /// </summary>
        public string Graphid { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string UnitName { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { get; set; }
        /// <summary>
        /// 需求人员
        /// </summary>
        public string PsnName { get; set; }
        /// <summary>
        /// 需求部门
        /// </summary>
        public string DeptName { get; set; }
        /// <summary>
        /// 需求组织
        /// </summary>
        public string ReqName { get; set; }
    }
}
