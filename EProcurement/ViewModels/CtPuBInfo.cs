﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;

namespace EProcurement.ViewModels
{
    /// <summary>
    /// 合同表体信息
    /// </summary>
    public class CtPuBInfo:CT_PU_B
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料型号
        /// </summary>
        public string MaterialType { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string UnitName { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 需求组织
        /// </summary>
        public string ReqName { get; set; }

        /// <summary>
        /// 需求组织简称
        /// </summary>
        public string ShortName { get; set; }
    }
}
