﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EProcurement.ViewModels
{
    public class EcPubInfo
    {
        /// <summary>
        /// 门户公告主键
        /// </summary>
        public string PK_INFOPUB_H { get; set; }

        /// <summary>
        /// 发布组织名称
        /// </summary>
        public string PURNAME { get; set; }

        /// <summary>
        /// 发布人员
        /// </summary>
        public string USENAME { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public string PUBTIME { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string INFOTITLE { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string INFO_CONTENT { get; set; }
    }
}
