﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;

namespace EProcurement.ViewModels
{
    /// <summary>
    /// 合同主体信息
    /// </summary>
    public class CtPuHInfo:CT_PU
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// 供应商地址
        /// </summary>
        public string SupplierAddr { get; set; }

        /// <summary>
        /// 供应商账户
        /// </summary>
        public string SupplierAcc { get; set; }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string OpenBankName { get; set; }

        /// <summary>
        /// 需求组织名称
        /// </summary>
        public string ReqOrgName { get; set; }

        /// <summary>
        /// 业务员名称
        /// </summary>
        public string PsnName { get; set; }
    }
}
