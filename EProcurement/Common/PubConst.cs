﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EProcurement
{
    public class PubConst
    {
        /// <summary>
        /// 用户身份信息
        /// </summary>
        public static string USERINFO = "UserInfo";

        public static string VERIFYCODE = "VerifyCode";
    }
}
