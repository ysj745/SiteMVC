﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.ViewModels;
using System.Web;

namespace EProcurement
{
    /// <summary>
    /// 登录用户信息辅助类
    /// </summary>
    public class UserHelper
    {
        public static string pk_supplier;
        /// <summary>
        /// 获取登录用户供应商身份
        /// </summary>
        /// <returns></returns>
        public static string GetPk_Supplier()
        {
            var oj = System.Web.HttpContext.Current.Session[PubConst.USERINFO];
            if (oj != null)
            {
                var sysUser = (SysUser)oj;
                //return sysUser.PK_SUPPLIER;
                return "1001B11000000004KNEZ";
            }
            return string.Empty;
        }

        /// <summary>
        ///  获取登录用户供应商身份
        /// </summary>
        public string Pk_Supplier {
            get
            {
                return GetPk_Supplier();
            }
        }

        /// <summary>
        /// 获取登录用户信息
        /// </summary>
        /// <returns></returns>
        public static SysUser GetUserInfo()
        {
            var oj = System.Web.HttpContext.Current.Session[PubConst.USERINFO];
            if (oj != null)
            {
                var sysUser = (SysUser)oj;
                return sysUser;
            }
            return null;
        }

        /// <summary>
        /// 获取用户主键
        /// </summary>
        /// <returns></returns>
        public static string GetCuserid()
        {
            var oj = System.Web.HttpContext.Current.Session["UserInfo"];
            if (oj != null)
            {
                var sysUser = (SysUser)oj;
                return sysUser.CUSERID;
            }
            return string.Empty;
        }

        /// <summary>
        /// 获取用户名称
        /// </summary>
        /// <returns></returns>
        public static string GetUserName()
        {
            var oj = System.Web.HttpContext.Current.Session["UserInfo"];
            if (oj != null)
            {
                var sysUser = (SysUser)oj;
                return sysUser.USER_NAME;
            }
            return string.Empty;
        }

    }
}
