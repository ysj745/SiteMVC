﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.ViewModels;
using EProcurement.DbModels;
using OracleSugar;

namespace EProcurement.Dao
{
    /// <summary>
    /// 用户管理
    /// </summary>
    public class UserManager
    {
        public UserManager()
        {

        }
        /// <summary>
        /// 已经编码获取用户的信息
        /// </summary>
        /// <param name="code">编码</param>
        /// <returns></returns>
        public static SysUser GetUserInfo(string code)
        {
            var sysUser = SugarDao.GetInstance().Queryable<SM_USER>()
                .Where<SM_USER>(s => s.USER_CODE == code)
                .Select<SysUser>(c=>new SysUser {CUSERID=c.CUSERID,USER_CODE=c.USER_CODE,PK_SUPPLIER=c.PK_SUPPLIER,
                USER_PASSWORD=c.USER_PASSWORD,USER_NAME=c.USER_NAME})
                .First<SysUser>();
            return sysUser;
        }

        /// <summary>
        /// 获取用户关联的供应商信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static SysUser GetSupplierInfo(string code)
        {
            return null;
        }
    }
}
