﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;
using EProcurement.ViewModels;
using OracleSugar;


namespace EProcurement.Dao
{
    /// <summary>
    /// 采购合同管理
    /// </summary>
    public class CtPuManager
    {
        /// <summary>
        /// 获取合同条款
        /// </summary>
        /// <param name="pk_ct_pu">合同主键</param>
        /// <returns></returns>
        public List<CT_PU_TERM> GetCtTerm(string pk_ct_pu)
        {
            if (string.IsNullOrEmpty(pk_ct_pu))
            {
                return null;
            }
            var data=SugarDao.GetInstance().Queryable<CT_PU_TERM>()
                .Where<CT_PU_TERM>(t => t.PK_CT_PU == pk_ct_pu && t.DR == 0)
                .ToList();
            return data;
        }

        /// <summary>
        /// 获取合同主体信息
        /// </summary>
        /// <param name="pk_ct_pu"></param>
        /// <returns></returns>
        public CtPuHInfo GetCtPu(string pk_ct_pu)
        {
            if (string.IsNullOrEmpty(pk_ct_pu))
            {
                return null;
            }
            var data = SugarDao.GetInstance().Queryable<CT_PU>()
                .JoinTable<BD_SUPPLIER>((h, s) => h.CVENDORID == s.PK_SUPPLIER)
                .JoinTable<BD_PSNDOC>((h,p)=>h.PK_PUBPSN==p.PK_PSNDOC)
                .Where<CT_PU>(h => h.PK_CT_PU == pk_ct_pu && h.DR == 0)
                .Select<CtPuHInfo>("h.*,s.name as suppliername,p.name as psnname")
                .First();
            return data;
        }

        /// <summary>
        /// 获取合同表体信息
        /// </summary>
        /// <param name="pk_ct_pu">合同主键</param>
        /// <returns></returns>
        public List<CtPuBInfo> GetCtPuB(string pk_ct_pu)
        {
            if (string.IsNullOrEmpty(pk_ct_pu))
            {
                return null;
            }
            var data = SugarDao.GetInstance().Queryable<CT_PU_B>()
                .JoinTable<BD_MATERIAL>((b, m) => b.PK_MATERIAL == m.PK_MATERIAL)
                .JoinTable<BD_MEASDOC>((b,d)=>b.CUNITID==d.PK_MEASDOC)
                .JoinTable<ORG_STOCKORG>((b,g)=>b.PK_ARRVSTOCK==g.PK_STOCKORG)
                .JoinTable<XF_BRANDDOC>((b,br)=>b.VBDEF3==br.PK_BRAND)
                .Where<CT_PU_B>(b => b.PK_CT_PU == pk_ct_pu && b.DR == 0)
                .Select<CtPuBInfo>("b.*,m.code as materialcode,m.name as materialname,m.materialspec,m.materialtype,d.name as unitname,g.name as reqname,g.shortname,br.brandname")
                .ToList();
            return data;
        }

        /// <summary>
        /// 获取待签合同列表信息
        /// </summary>
        /// <returns></returns>
        public Queryable<CtPuHInfo> GetCtPuList()
        {
            var pk_supplier = UserHelper.GetPk_Supplier();
            var data = SugarDao.GetInstance().Queryable<CT_PU>()
                .JoinTable<BD_SUPPLIER>((h, s) => h.CVENDORID == s.PK_SUPPLIER)
                .JoinTable<BD_PSNDOC>((h, p) => h.PK_PUBPSN == p.PK_PSNDOC)
                .Where<CT_PU>(h => h.CVENDORID == pk_supplier && h.DR == 0)
                 .OrderBy<CT_PU>(h => h.VBILLCODE, OrderByType.Desc)
                 .Take(10)
                .Select<CtPuHInfo>("h.*,s.name as suppliername,p.name as psnname");
            return data;
        }

        /// <summary>
        /// 获取已签合同列表信息（最近6个月）
        /// </summary>
        /// <returns></returns>
        public Queryable<CtPuHInfo> GetCtPuListLast()
        {
            var pk_supplier = UserHelper.GetPk_Supplier();
            var data = SugarDao.GetInstance().Queryable<CT_PU>()
                .JoinTable<BD_SUPPLIER>((h, s) => h.CVENDORID == s.PK_SUPPLIER)
                .JoinTable<BD_PSNDOC>((h, p) => h.PK_PUBPSN == p.PK_PSNDOC)
                .Where<CT_PU>(h => h.CVENDORID == pk_supplier && h.DR == 0)
                 .OrderBy<CT_PU>(h => h.VBILLCODE, OrderByType.Desc)
                 .Take(30)
                .Select<CtPuHInfo>("h.*,s.name as suppliername,p.name as psnname");
            return data;
        }
    }
}
