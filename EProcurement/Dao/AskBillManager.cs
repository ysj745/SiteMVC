﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.ViewModels;
using EProcurement.DbModels;
using OracleSugar;
using Utils;

namespace EProcurement.Dao
{
    /// <summary>
    /// 询报价管理
    /// </summary>
    public class AskBillManager
    {
        /// <summary>
        /// 获取未报价数据
        /// </summary>
        public Queryable<AskBillBInfo> GetUnAskBill()
        {
            var pk_supplier = UserHelper.GetPk_Supplier();//供应商主键
            var data = SugarDao.GetInstance().Queryable<PURP_ASKBILL_B>()
                .JoinTable<PURP_ASKBILL>((b,h)=>b.PK_ASKBILL==h.PK_ASKBILL)
               .JoinTable<ORG_STOCKORG>((b, g) => b.VBDEF8 == g.PK_STOCKORG)
               .JoinTable<BD_MATERIAL>((b, l) => b.PK_MATERIAL == l.PK_MATERIAL)
               .JoinTable<BD_PSNDOC>((b, p) => b.VBDEF5 == p.PK_PSNDOC)
               .JoinTable<ORG_DEPT>((b, t) => b.VBDEF7 == t.PK_DEPT)
               .JoinTable<BD_MEASDOC>((b, me) => b.CUNITID == me.PK_MEASDOC)
               .JoinTable<XF_BRANDDOC>((b, br) => b.VBDEF3 == br.PK_BRAND)
               .Where<ORG_STOCKORG,PURP_ASKBILL>((b, g,h) => b.DR == 0&&h.VDEF3=="1"&&b.FBILLSTATUS==2
                && b.VBDEF16 == "1" && b.VBDEF10 == "Y" 
                && b.PK_SUPPLIER == pk_supplier)
               .Select<AskBillBInfo>(@"b.pk_askbill_b,h.vbillcode,l.name as materialname,l.materialspec,l.materialtype,l.graphid,br.brandname,p.name as psnname,t.name as deptname,b.vbdef1,g.name as reqname,b.nnum,me.name as unitname,b.nastnum,b.vbdef11,b.vbdef12,b.vbdef13,b.vbdef14");
            return data;
        }

        /// <summary>
        /// 更新报价信息
        /// </summary>
        /// <param name="askBill_b"></param>
        public bool UpdateQuoteInfo(PURP_ASKBILL_B askBill_b)
        {
            var pk_supplier = UserHelper.GetPk_Supplier();//供应商主键
            var nastorigprice = (100 - askBill_b.NTAXRATE)/100* decimal.Parse(askBill_b.VBDEF11);//无税单价
            var norigtaxmny = Math.Round(decimal.Parse(askBill_b.VBDEF11) * askBill_b.NNUM, 2).ToString();//价税合计
            return SugarDao.GetInstance().Update<PURP_ASKBILL_B>(
            new {
                VBDEF11 = askBill_b.VBDEF11,//报价含税单价
                NTAXPRICE=askBill_b.VBDEF11,//议价含税单价
                NTAXRATE = askBill_b.NTAXRATE,//税率
                VBDEF2 = nastorigprice.ToString(),//报价无税单价
                VBDEF12 = norigtaxmny,//报价价税合计
                VBDEF13 = askBill_b.VBDEF13,//报价交货周期（天）
                DELIVERDAYS=askBill_b.VBDEF13,//议价交货周期（天）
                VBDEF14 = askBill_b.VBDEF14,//报价质保期(月)
                VBDEF4 = askBill_b.VBDEF14,//议价质保期(月)
                VBDEF15 = CommonHelper.GetIP(),
                VBDEF16 = "1",//报价方式1=未响应,2=线上报价
                FBILLSTATUS ="2",//报价状态0=自由，1=完成，2=发出，3=报价，测试完成后改为3
                VBDEF19 = askBill_b.VBDEF19,//反馈不报价
                VBDEF10="0",
                TS=DateTime.Now.ToString()
            }, b => b.PK_ASKBILL_B == askBill_b.PK_ASKBILL_B&&b.PK_SUPPLIER== pk_supplier);
        }
    }
}
