﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EProcurement.DbModels;
using EProcurement.ViewModels;
using OracleSugar;

namespace EProcurement.Dao
{
    /// <summary>
    /// 供应商门户公告信息
    /// </summary>
    public class EcInfoManager
    {
        public EcInfoManager(){
        }

        /// <summary>
        /// 获取发布的公告信息(前十条)
        /// </summary>
        /// <returns></returns>
        public List<EcPubInfo> GetECInfo()
        {
           var take = SugarDao.GetInstance().Queryable<EC_INFOPUB_H>()
                .JoinTable<SM_USER>((h,u)=>h.PUBOPERATOR==u.CUSERID)
                .JoinTable<ORG_PURCHASEORG>((h,g)=>h.PK_ORG==g.PK_ORG)
                .Where(h => h.DR==0&&h.INFO_STATUS==6&&h.INFOTYPE==2&&h.INFOPUBDOMAIN==3)
                .OrderBy(h => h.TS,OrderByType.Desc)
                .Select<SM_USER, ORG_PURCHASEORG, EcPubInfo>((h,u,g)=>new EcPubInfo { PK_INFOPUB_H=h.PK_INFOPUB_H,PURNAME=g.NAME,USENAME=u.USER_NAME,
                PUBTIME=h.PUBTIME,INFOTITLE=h.INFOTITLE,INFO_CONTENT=h.INFO_CONTENT})
                .Take(10).ToList();
            return take;
        }

        /// <summary>
        /// 获取公告信息
        /// 依据ID
        /// </summary>
        /// <param name="id">公告信息主键</param>
        /// <returns></returns>
        public EcPubInfo GetECInfoByID(string id)
        {
            var ecInfo = SugarDao.GetInstance().Queryable<EC_INFOPUB_H>()
                 .JoinTable<SM_USER>((h, u) => h.PUBOPERATOR == u.CUSERID)
                 .JoinTable<ORG_PURCHASEORG>((h, g) => h.PK_ORG == g.PK_ORG)
                 .Where(h => h.DR == 0 && h.INFO_STATUS == 6 && h.INFOTYPE == 2 && h.INFOPUBDOMAIN == 3)
                 .Where(h => h.PK_INFOPUB_H == id)
                 .Select<SM_USER, ORG_PURCHASEORG, EcPubInfo>((h, u, g) => new EcPubInfo
                 {
                     PK_INFOPUB_H = h.PK_INFOPUB_H,
                     PURNAME = g.NAME,
                     USENAME = u.USER_NAME,
                     PUBTIME = h.PUBTIME,
                     INFOTITLE = h.INFOTITLE,
                     INFO_CONTENT = h.INFO_CONTENT
                 }).First();
            return ecInfo;
        }
    }
}
