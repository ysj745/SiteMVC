﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EProcurement;
using EProcurement.Dao;
using EProcurement.ViewModels;
using SyntacticSugar;
using SiteMVC.Models;
using SiteMVC.Filters;

namespace SiteMVC.Controllers
{
    [NotVerify]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //获取公告内容
            EcInfoManager info = new EcInfoManager();
            ViewBag.EcInfos=info.GetECInfo();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// 查看公告详细信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ViewInfo(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index", "Home");
            }
            //获取公告内容
            EcInfoManager info = new EcInfoManager();
            ViewBag.EcInfo = info.GetECInfoByID(id);
            return View();
        }

        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="usercode"></param>
        /// <param name="password"></param>
        /// <param name="verifycode"></param>
        public ActionResult Login(string usercode,string password,string verifycode)
        {
            ResultModel<string> result = new ResultModel<string>();
            if (string.IsNullOrEmpty(verifycode) || Session[PubConst.VERIFYCODE] ==null|| !Session[PubConst.VERIFYCODE].Equals(verifycode))
            {
                result.IsSuccess = false;
                result.ResultInfo = "验证码为空或错误";
            }
            else
            {
                if (string.IsNullOrEmpty(usercode) || string.IsNullOrEmpty(password))
                {
                    result.IsSuccess = false;
                    result.ResultInfo = "用户名或密码为空";
                }
                else
                {
                    SysUser userInfo = UserManager.GetUserInfo(usercode);
                    if (userInfo == null)
                    {
                        result.IsSuccess = false;
                        result.ResultInfo = "用户名或密码错误";
                    }
                    else
                    {
                        //清空验证码
                        Session[PubConst.VERIFYCODE] = null;
                        String pwdparm = "U_U++--V" + EncryptSugar.GetInstance().MD5(userInfo.CUSERID + password).ToLower();
                        if (pwdparm.Equals(userInfo.USER_PASSWORD))
                        {
                            result.IsSuccess = true;
                            Session[PubConst.USERINFO] = userInfo;
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.ResultInfo = "用户名或密码错误";
                        }
                    }
                }
            }
            return Json(result);
        }

        /// <summary>
        /// 获取验证码图片
        /// </summary>
        public void VerifyCode()
        {
            VerifyCodeSugar v = new VerifyCodeSugar();
            //是否随机字体颜色
            v.SetIsRandomColor = true;
            //随机码的旋转角度
            v.SetRandomAngle = 4;
            //文字大小
            v.SetFontSize = 15;
            //背景色
            //v.SetBackgroundColor
            //前景噪点数量
            //v.SetForeNoisePointCount = 3;
            //v.SetFontColor =Color.Red;
            //...还有更多设置不介绍了
            var questionList = new Dictionary<string, string>()
           {
                {"1+1=？","2"},
               {"喜羊羊主角叫什么名字？","喜羊羊" },
               {"【我爱你】中间的那个字？","爱" },
           };
            var questionItem = v.GetQuestion(questionList);//不赋值为随机验证码 例如： 1*2=? 这种
                                                           //指定验证文本
                                                           //v.SetVerifyCodeText
            v.SetVerifyCodeText = questionItem.Key;
            Session[PubConst.VERIFYCODE] = questionItem.Value;
            //输出图片
            v.OutputImage(System.Web.HttpContext.Current.Response);
        }
    }
}