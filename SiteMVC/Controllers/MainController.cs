﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EProcurement;
using EProcurement.Dao;
using EProcurement.ViewModels;
using EProcurement.DbModels;
using SyntacticSugar;
using SiteMVC.Models;
using SiteMVC.Filters;
using System.IO;
using OracleSugar;

namespace SiteMVC.Controllers
{
    /// <summary>
    /// 登录后主界面
    /// </summary>
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            //获取待报价条数
            AskBillManager askBill = new AskBillManager();
            ViewBag.UnAskCounts= askBill.GetUnAskBill().Count();
            //获取合同内容
            CtPuManager puManager = new CtPuManager();
            ViewBag.CtCounts = puManager.GetCtPuList().Count();
            return View();
        }
    }
}