﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EProcurement;
using EProcurement.Dao;
using EProcurement.ViewModels;
using EProcurement.DbModels;
using SyntacticSugar;
using SiteMVC.Models;
using SiteMVC.Filters;
using System.IO;
using OracleSugar;

namespace SiteMVC.Controllers
{
    /// <summary>
    /// 合同管理
    /// </summary>
    public class CtPuController : Controller
    {

        public ActionResult Index()
        {
            //获取合同内容
            CtPuManager puManager = new CtPuManager();
            var ctHinfos= puManager.GetCtPuList().ToList();
            ViewBag.CtHInfos = ctHinfos;
            //ViewBag.CtBInfos = puManager.GetCtPuB(pk_ct_pu);
            //条数
            ViewBag.CtCount = ctHinfos.Count;
            ViewBag.AlreadyCtCount = puManager.GetCtPuListLast().Count();
            return View();
        }

        /// <summary>
        /// 已经签订合同（最近6个月）
        /// </summary>
        /// <returns></returns>
        public ActionResult AlreadyCt()
        {
            //获取合同内容
            CtPuManager puManager = new CtPuManager();
            var ctHinfos = puManager.GetCtPuListLast().ToList();
            ViewBag.CtHInfos = ctHinfos;
            //ViewBag.CtBInfos = puManager.GetCtPuB(pk_ct_pu);
            //条数
            ViewBag.CtCount = puManager.GetCtPuList().Count();
            ViewBag.AlreadyCtCount = ctHinfos.Count;
            return View();
        }

        /// <summary>
        /// 保存pdf
        /// </summary>
        /// <returns></returns>
        public ActionResult CratePdf(string id)
        {
            ResultModel<string> result = new ResultModel<string>();
            if (string.IsNullOrEmpty(id))
            {
                result.IsSuccess = false;
                result.ResultInfo = "创建失败";
                return Json(result);
            }
            string path = Server.MapPath("~/Pdf");//设定上传的文件路径
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string pk_ct_pu = id;// "1001B110000000BLNDQA";
            //获取合同内容
            CtPuManager puManager = new CtPuManager();
            ViewBag.CtHInfo = puManager.GetCtPu(pk_ct_pu);
            ViewBag.CtBInfos = puManager.GetCtPuB(pk_ct_pu);
            ViewBag.Terms = puManager.GetCtTerm(pk_ct_pu);
            if (ViewBag.CtHInfo==null|| ViewBag.CtBInfos==null|| ViewBag.CtBInfos==null)
            {
                result.IsSuccess = false;
                result.ResultInfo = "获取数据失败";
                return Json(result);
            }

            var content = this.ControllerContext.GetViewHtml("pdf", null);
            Utils.CommonHelper.CreatePdf(path, id+".pdf", content);
            //FTP合同PDF到签章服务器
            Utils.FTPHelper ftpHelper = new Utils.FTPHelper("10.10.4.206",null,"nc","abc123");
            ftpHelper.Upload(path + "/" + id + ".pdf");
            result.IsSuccess = true;
            result.ResultInfo = "创建成功";
            return Json(result);
        }

        /// <summary>
        /// 预览pdf
        /// </summary>
        /// <returns></returns>
        public FileContentResult PreviewPdf(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.Msg = "预览失败";
            }
             string pk_ct_pu = id;
            //获取合同内容
            CtPuManager puManager = new CtPuManager();
            ViewBag.CtHInfo = puManager.GetCtPu(pk_ct_pu);
            ViewBag.CtBInfos = puManager.GetCtPuB(pk_ct_pu);
            ViewBag.Terms = puManager.GetCtTerm(pk_ct_pu);

            if (ViewBag.CtHInfo == null || ViewBag.CtBInfos == null || ViewBag.CtBInfos == null)
            {
                ViewBag.Msg = "获取数据失败";
            }
            var content = this.ControllerContext.GetViewHtml("pdf", null);
            var barry = Utils.CommonHelper.ConvertHtmlTextToPDF(content);
            return File(barry, "application/pdf");
        }
    }
}