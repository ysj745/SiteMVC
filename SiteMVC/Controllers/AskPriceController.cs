﻿using EProcurement.Dao;
using EProcurement.DbModels;
using SiteMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OracleSugar;

namespace SiteMVC.Controllers
{
    /// <summary>
    /// 询报价
    /// </summary>
    public class AskPriceController : Controller
    {
        // GET: AskPrice
        public ActionResult Index()
        {
            //获取待报价信息
            AskBillManager askBill = new AskBillManager();
            ViewBag.UnAskBills = askBill.GetUnAskBill().ToList();
            return View();
        }

        /// <summary>
        /// 更新报价
        /// </summary>
        /// <param name="askBill"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateQuoteInfo(PURP_ASKBILL_B askBill_b)
        {
            //校验返回数据的安全性

            AskBillManager askBill = new AskBillManager();
            ResultModel<string> result = new ResultModel<string>();
            result.IsSuccess = askBill.UpdateQuoteInfo(askBill_b);
            if (!result.IsSuccess)
            {
                result.ResultInfo = "更新报价失败";
            }
            return Json(result);
        }
    }
}