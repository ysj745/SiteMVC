﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteMVC.Filters
{
    /// <summary>
    /// 权限验证
    /// </summary>
    public class LoginAuthorizeAttribute : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var areaName = filterContext.RouteData.DataTokens["area"];
            var controllerName = filterContext.RouteData.Values["controller"];
            var action = filterContext.RouteData.Values["Action"];
            ///此处绕过权限验证采用了两种方式：
            /// 1、直接判断控制器或者方法上面是否有不进行权限验证的描述
            /// 2、判断路由地址
    
            //判断是否有NotVerifyAttribute属性描述
            if (filterContext.ActionDescriptor.IsDefined(typeof(NotVerifyAttribute), false) || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(NotVerifyAttribute), false))
            {
                return;
            }
            else
            {
                var userInfo = filterContext.HttpContext.Session["UserInfo"];
                if (userInfo == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new HttpStatusCodeResult(499, "您没有该权限");
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Home/Index");
                    }
                }
                else
                {
                    //判断具体的功能节点权限
                }
            }
        }
    }
}