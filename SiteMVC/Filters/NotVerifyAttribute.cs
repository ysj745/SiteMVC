﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteMVC.Filters
{
    /// <summary>
    /// 不进行权限验证属性
    /// 在对应的控制器、方法上加上此描述
    /// </summary>
    public class NotVerifyAttribute:Attribute
    {
    }
}