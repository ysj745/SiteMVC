﻿using System.Web;
using System.Web.Mvc;
using SiteMVC.Filters;

namespace SiteMVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //权限验证
            filters.Add(new LoginAuthorizeAttribute());
        }
    }
}
