﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteMVC.Models
{
    /// <summary>
    /// 接口统一返回类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultModel<T>
    {
        public T ResultInfo { get; set; }

        public bool IsSuccess { get; set; }

        public string Status { get; set; }
    }
}