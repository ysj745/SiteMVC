﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Utils
{
    /// <summary>
    /// 通用工具类
    /// </summary>
    public class CommonHelper
    {
        /// <summary>
        /// 处理字符串：如果字符串的值为非空的“~”，则直接返回，否则返回空。
        /// </summary>
        /// <returns></returns>
        public static string RemoveTStr(string str)
        {
            if (!string.IsNullOrEmpty(str) && str.Equals("~"))
            {
                return string.Empty;
            }
            else
            {
                return str;
            }
        }
        /// <summary>
        /// 获取客户端IP地址
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        public static string GetIP()
        {
            string result = String.Empty;
            result = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //可能有代理   
            if (!string.IsNullOrWhiteSpace(result))
            {
                //没有"." 肯定是非IP格式  
                if (result.IndexOf(".") == -1)
                {
                    result = null;
                }
                else
                {
                    //有","，估计多个代理。取第一个不是内网的IP。  
                    if (result.IndexOf(",") != -1)
                    {
                        result = result.Replace(" ", string.Empty).Replace("\"", string.Empty);

                        string[] temparyip = result.Split(",;".ToCharArray());

                        if (temparyip != null && temparyip.Length > 0)
                        {
                            for (int i = 0; i < temparyip.Length; i++)
                            {
                                //找到不是内网的地址  
                                if (IsIPAddress(temparyip[i]) && temparyip[i].Substring(0, 3) != "10." && temparyip[i].Substring(0, 7) != "192.168" && temparyip[i].Substring(0, 7) != "172.16.")
                                {
                                    return temparyip[i];
                                }
                            }
                        }
                    }
                    //代理即是IP格式  
                    else if (IsIPAddress(result))
                    {
                        return result;
                    }
                    //代理中的内容非IP  
                    else
                    {
                        result = null;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            return result;
        }

        /// <summary>
        /// 检测是否是IP地址
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsIPAddress(string str)
        {
            if (string.IsNullOrWhiteSpace(str) || str.Length < 7 || str.Length > 15)
                return false;

            string regformat = @"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})";
            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);

            return regex.IsMatch(str);
        }

        /// <summary>
        /// 创建PDF
        /// </summary>
        /// <param name="path">pdf保存路径</param>
        /// <param name="fileName">PDF保存名称</param>
        /// <param name="_str">html内容</param>
        public static void CreatePdf(string path, string fileName, string _str)
        {
            //定义一个Document，并设置页面大小为A4，竖向 
            iTextSharp.text.Document doc = new Document(PageSize.A4);
            try
            {
                //写实例 
                var writer = PdfWriter.GetInstance(doc, new FileStream(path + "/" + fileName, FileMode.Create));
                #region 设置PDF的头信息，一些属性设置，在Document.Open 之前完成
                //doc.AddAuthor("作者信发集团");
                doc.AddCreationDate();
                doc.AddCreator("创建人信发集团");
                //doc.AddSubject("Dot Net 使用 itextsharp 类库创建PDF文件的例子");
                //doc.AddTitle("此PDF由幻想Zerow创建，嘿嘿");
                //doc.AddKeywords("ASP.NET,PDF,iTextSharp,幻想Zerow");
                //自定义头 
                doc.AddHeader("Expires", "0");
                #endregion //打开document
                doc.Open();

                //写入信息
                byte[] array = System.Text.Encoding.UTF8.GetBytes(_str);
                MemoryStream stream = new MemoryStream(array);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, stream, (Stream)null, System.Text.Encoding.UTF8, new SongFontFactory());

                doc.Close();
            }
            catch (DocumentException de) {
                Console.WriteLine(de.Message); Console.ReadKey();
            }
            catch (IOException io) {
                Console.WriteLine(io.Message); Console.ReadKey();
            }
        }

        /// <summary>
        /// 创建PDF文档流
        /// </summary>
        /// <param name="htmlText">html内容</param>
        /// <returns></returns>
        public static byte[] ConvertHtmlTextToPDF(string htmlText)
        {
            if (string.IsNullOrEmpty(htmlText))
            {
                return null;
            }
            //避免當htmlText無任何html tag標籤的純文字時，轉PDF時會掛掉，所以一律加上<p>標籤
            htmlText = "<p>" + htmlText + "</p>";

            MemoryStream outputStream = new MemoryStream();//要把PDF寫到哪個串流
            byte[] data = Encoding.UTF8.GetBytes(htmlText);//字串轉成byte[]
            MemoryStream msInput = new MemoryStream(data);
            Document doc = new Document();//要寫PDF的文件，建構子沒填的話預設直式A4
            PdfWriter writer = PdfWriter.GetInstance(doc, outputStream);
            //指定文件預設開檔時的縮放為100%
            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
            //開啟Document文件 
            doc.Open();

            //使用XMLWorkerHelper把Html parse到PDF文档
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8, new SongFontFactory());
            //將pdfDest設定的資料寫到PDF檔
            PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);
            writer.SetOpenAction(action);
            doc.Close();
            msInput.Close();
            outputStream.Close();
            //回傳PDF檔案 
            return outputStream.ToArray();
        }
    }
    /// <summary>
    /// 重写iTextSharp字体(仅仅使用宋体)
    /// </summary>
    public class SongFontFactory : IFontProvider
    {
        public Font GetFont(String fontname, String encoding, Boolean embedded, float size, int style, BaseColor color)
        {

            BaseFont bf3 = BaseFont.CreateFont(@"c:\windows\fonts\SIMHEI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font fontContent = new Font(bf3, size, style, color);
            return fontContent;

        }
        public Boolean IsRegistered(String fontname)
        {
            return false;
        }
    }
}
